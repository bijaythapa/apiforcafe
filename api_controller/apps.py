# from __future__ import absolute_import, unicode_literals
from django.apps import AppConfig


class ApiControllerConfig(AppConfig):
    name = 'api_controller'
